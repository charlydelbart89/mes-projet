<?php
// on outorise les requetes ajax pour toutes les sources 
header('Access-Control-Allow-Origin: *');
// on verifie qu'on a bien la méthode POST
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    // je suis en method post
    if(isset($_POST['pseudo']) && !empty($_POST['pseudo']) && isset($_POST['message']) && !empty($_POST['message'])){
        // on se connecte a la base
        require_once('connect.php');

        // on sécurise en suppriment le risque d'injestion xss
        $pseudo =strip_tags(htmlentities($_POST['pseudo']));
        $message =strip_tags(htmlentities($_POST['message']));

        $sql = 'INSERT INTO `message`(`pseudo`, `message`) VALUE
        (:pseudo, :message)';
        $query = $db -> prepare($sql);
        $query -> bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
        $query -> bindValue(':message', $message, PDO::PARAM_STR);
        if($query -> execute()){
            // ici la creation a fonctionné
            http_response_code(201);
            echo "Ajout effectué";
        }else{
            // ici la creation a echoué
            http_response_code(503);
            echo "Echec";
        }
        // on se deconnecte de la base
        require_once('close.php');
    }
}else{
    http_response_code(405);
    echo "La methode n'est pas autorisée";
}

    ?>