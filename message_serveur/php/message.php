<?php
// on outorise les requetes ajax pour toutes les sources 
header('Access-Control-Allow-Origin: *');

// on utilise la metode get

// echo '<pre>';
// var_dump($_SERVER);
if($_SERVER['REQUEST_METHOD'] == 'GET'){
    // ici on utilise la methode get
    // on se connecte a la base
    require_once('connect.php');

    // on recupere les donées de la base
    $sql = 'SELECT * FROM `message`';
    $query = $db -> query($sql);
    $message = $query -> fetchAll();

    // on envoie le code de la base
    http_response_code(200);

    // on envoie les donée en json 
    echo json_encode($message);
    // on se deconnect de la base
    require_once('close.php');
}else{
    http_response_code(405);
    echo "La methode n'est pas autorisée";
}
