<?php
// on outorise les requetes ajax pour toutes les sources 
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Origin: *');

// on utilise la metode get

if($_SERVER['REQUEST_METHOD'] == 'DELETE'){

    if(isset($_GET['id']) && !empty($_GET['id'])){
        // ici on a un id non vide
        require_once("connect.php");

        $id = strip_tags(htmlentities($_GET['id']));

        $sql = 'DELETE FROM `message` WHERE `id` = :id';
        $query = $db -> prepare($sql);
        $query -> bindValue(':id', $id, PDO::PARAM_INT);

        if($query -> execute()){
            http_response_code(200);
            echo 'Suppression effectuée';
        }else{
            http_response_code(503);
            echo 'Echec';
        }
    }require_once('close.php');
    
}
else if($_SERVER['REQUEST_METHOD'] != 'OPTIONS'){
    http_response_code(405);
    echo 'mauvais maithode';
}
?>