// // on crée un nouvelle objet xmlhttprequest 
// let xmlhttp = new XMLHttpRequest();

// xmlhttp.onreadystatechange = function(){
//     if(xmlhttp.status == 200){
//         // l'url est valide
//         if(xmlhttp.readyState == 4){
//             console.log(xmlhttp.response);
//         }
//     }else{
//         // une erreur c'est produite
//         console.log("une erreur est survenue");
//     }
// };

// // on ouvre la connexion
// xmlhttp.open("GET", "http://serveurajax.test/lire.php", true);

// // on envoie la requete
// xmlhttp.send();

// console.log(xmlhttp);

$(function(){
    // ici on sait que le dom est chaegé
    $("#liste").on("change", chargerClient);
});

// meme chose avec jquery
$.ajax({
    url: "http://serveurajax.test/lire.php"
}).done(function(reponse){
    // ici l'url est trouvée et les donnée ont ete recues
    let donnees = JSON.parse(reponse);
    // on boucle sur le tableau
    $.each(donnees, function(id, client){
        $("#liste").append(`<option value="${client.id}">${client.nom}</option`)
    });

}).fail(function(erreur){
    // ici un probleme est survenue
    console.log(erreur);
});


/**
 * cette fonction charge un client et l'affiche sous le select
 */
function chargerClient(){
    // on recupere la valeur du select
    let id = $("#liste").val();

    if(id == ""){
        $("#client").html("");
    }else{
    // on change 1 client
    $.ajax({
        url: "http://serveurajax.test/client.php",
        data: {"id": id}
    }).done(function(reponse){
        let donnees = JSON.parse(reponse);
        $("#client").html(`<p>${donnees.prenom} ${donnees.nom}</p>`);
    }).fail(function(erreur){
        console.log(erreur);
    });
}
}