$('.carousel').carousel({
    interval:3000
})



$(function(){
    $(document).on("scroll", barre);


    $('.owl-carousel').owlCarousel({
        autoplayTimeout:2000,
        autoplay:true,
        loop:true,
        margin:10,
        nav:true,
        center:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
        
    }) // fermeture de owl

    $('.owl-carousel').on('changed.owl.carousel', function(){

    

        $(".owl-carousel .owl-item").css("transform", "scale(1)");
        $(".owl-carousel .center").next().css("transform", "scale(1.5)");
    })
}); // fermeture de window onload

function barre(){
    let position = $(document).scrollTop();
    console.log(position);

    if(position > 0){
        $(".navbar-light").css({
            "background-color": "white",
            "border": "1px solid black"});
    }else{
        $(".navbar-light").css({
            "background-color": "transparent",
            "border": "none"});

    }
}

