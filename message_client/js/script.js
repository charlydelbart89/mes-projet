$(function(){
    // ici le dom est chargé
    $("#valid").on("click", envoieMessage);
    $("#texte").on("keyup",entrer);
});


// meme chose avec jquery
setInterval(chargerMessage, 500);

/**
 * cette fonfonction rafraichit la liste des messages
 */
function chargerMessage(){
$.ajax({
    url: "http://message_serveur.test/php/message.php"
}).done(function(reponse){
    // ici l'url est trouvée et les donnée ont ete recues
    let message = JSON.parse(reponse);

    // on efface la liste des messages
    $("#discussion").html("");
    // on boucle sur le tableau
    $.each(message, function(id, message){
        $("#discussion").prepend(`<p> ${message.pseudo} :  ${message.message}</p>`)
    
    })
}).fail(function(erreur){
    // ici un probleme est survenue
    console.log(erreur);
});
}

function envoieMessage(){
    let pseudo = $("#pseudo").val();
    let message = $("#texte").val();

    // on verifie si les champs ne sont pas vides
    if(pseudo != "" && message != ""){
        $.ajax({
            url: "http://message_serveur.test/php/serveur.php",
            type: "post",
            data: {"pseudo": pseudo, "message": message}
        }).done(function(reponse){
            $("#texte").val("");
        }).fail(function(erreur){
            console.log(erreur);
        });
    }
}
function entrer(e){
    if(e.code == "Enter"){
        envoieMessage();
    }
}