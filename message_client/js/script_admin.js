$(function(){
    chargeTableau();
});

/**
 * cete fonction charge et rafraichit le tableau
 */
function chargeTableau(){

    $.ajax({
        url: "http://message_serveur.test/php/message.php"
    }).done(function(reponse){
        // ici l'url est trouvée et les donnée ont ete recues
        let message = JSON.parse(reponse);
    
        // on efface la liste des messages
        $("#liste").html("");
        // on boucle sur le tableau
        $.each(message, function(id, message){
            $("#liste").prepend(`<tr><td>${message.pseudo}</td><td>${message.message}</td><td><a href="#" class="btn 
            btn-danger" data-id="${message.id}"><i class="fas fa-times"></i></a></td></tr>`)
    
        });

        // ici on est certains que les balises <a> existent
    $("a").on("click", suppremessage);
}).fail(function(erreur){
    // ici un probleme est survenue
    console.log(erreur);
});
}

function suppremessage(){
    // on recupere l'id du message a supprimer (stocke dans data-id)
    let id = $(this).data("id");
    console.log(id);

    // si on a un id, on supprime
    if(id !=""){
        $.ajax({
            url: "http://message_serveur.test/php/suppr.php?id="+id,
            type: "delete"
        }).done(function(reponse){
            chargeTableau();
        }).fail(function(erreur){
            console.log(erreur);
        });
    }
}