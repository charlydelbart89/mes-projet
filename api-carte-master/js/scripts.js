let macarte = L.map('map').setView([47.797, 3.5852], 5);

// on initialise un tableau pour les marqueurs
let tableauMarqueurs = [];

// on ajoute les tuiles
L.tileLayer("https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png", {
    attribution: "Carte fournie par wiki media",
    maxZoom: 20,
    minZoom: 3
}).addTo(macarte);

let icone = L.icon({
    iconUrl: "img/logo.png",
    iconSize: [50, 50],
    iconAnchor: [23, 10]
})
let regroupe = L.markerClusterGroup();

let delimitation = L.marker([45.502212, -73.591115]).addTo(macarte);


// on ajout un marqueur sur la gare d'auxerre
// let marqueur = L.marker([47.79808908071206, 3.585019188334606]).addTo(macarte);
// let marqueur2 = L.marker([47.796607, 3.568705]).addTo(macarte);
// let marqueur2 = L.circle([47.796607, 3.568705], {
// 	color: 'red',
// 	fillColor: '#f03',
// 	fillOpacity: 0.4,
// 	radius: 100
// }).addTo(macarte);

// fair un popup
// marqueur.bindPopup("<img src='img/img1.jpg'></img><h1>Gare d'auxerre</h1><p>bienvenue.</p>");
// marqueur2.bindPopup("<h1>43 rue de Paris </h1><p>c'est chez moi.</p>")

delimitation.bindPopup("<p>voici le lieu de mon stge.</P><p>pour 6 mois.</p>")

navigator.geolocation.getCurrentPosition(success,echec)

function success(position){
    console.log(position);
    let marqueur = L.marker([position.coords.latitude,position.coords.longitude],).addTo(macarte);
    marqueur.bindPopup("<p>ici</p>")
}
function echec(){
    console.log("Refus");
}



    $.ajax({
        url: "https://nouvelle-techno.fr/dist/liste"
    })
    .done(function(reponse){
        let agences = JSON.parse(reponse);

        $.each(agences, function(id, agence){
            let marqueur = L.marker([agence.lat, agence.lon], {icon:icone})
            marqueur.bindPopup(`<h1> ${agence.nom} </h1><p>  ${agence.description}  </p>`);
            tableauMarqueurs.push(marqueur);
            regroupe.addLayer(marqueur);
        });
        macarte.addLayer(regroupe);

        // on pass le groupe de marqueurs a leaflet
        let groupe = new L.featureGroup(tableauMarqueurs);

        // on determine les limites nord, sud, est, ouest de nos marqueurs
        let limites = groupe.getBounds();

        // on adapte le zoom pour que les limite soit visible
        macarte.fitBounds(limites);

        // 
        console.log(limites);
    }).fail(function(erreur){
        console.log(erreur);
    });

    $.ajax({
        url: "https://nouvelle-techno.fr/dist/departements"
    }).done(function(reponse){
        let departements = JSON.parse(reponse);

        $.each(departements, function(id, departement){
            $("#dept").append(`<option value="${departement.dept}">${departement.dept}</option>`)
        });

    }).fail(function(erreur){
        console.log(erreur);
    });